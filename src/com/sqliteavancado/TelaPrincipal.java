package com.sqliteavancado;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

import com.sqliteavancado.dominio.Pessoa;

public class TelaPrincipal extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_principal);
    }

    
    public void listar(View v){
    	
    	
    	
    	ArrayList<Pessoa> pessoas = new ArrayList<Pessoa>();
    	
    	Intent intent = new Intent(this, Listagem.class);
    	intent.putExtra("pessoas", pessoas);
    	
    	startActivity(intent);
    	
    	
    	
    }
    
    
    
    
    
    
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_tela_principal, menu);
        return true;
    }

    
}
