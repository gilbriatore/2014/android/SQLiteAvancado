package com.sqliteavancado.persistencia;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.sqliteavancado.dominio.Pessoa;

public class CadastroDePessoa {
	
	private MinhaHelper helper;
	
	public CadastroDePessoa(Context contexto) {
		this.helper = new MinhaHelper(contexto);		
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		this.helper.close();
	}
	
	
	public void incluir(Pessoa pessoa){
		ContentValues valores = new ContentValues();
		valores.put(Pessoa.NOME, pessoa.getNome());
		valores.put(Pessoa.SOBRENOME, pessoa.getSobrenome());
		
		SQLiteDatabase db = helper.getWritableDatabase();
		db.insert(Pessoa.PESSOA, null, valores);
		db.close();		
	}
	
	public void atualizar(Pessoa pessoa){
		ContentValues valores = new ContentValues();
		valores.put(Pessoa.NOME, pessoa.getNome());
		valores.put(Pessoa.SOBRENOME, pessoa.getSobrenome());
		
		String[] id = {String.valueOf(pessoa.getId())};
		
		SQLiteDatabase db = helper.getWritableDatabase();
		db.update(Pessoa.PESSOA, valores, Pessoa.ID + "= ?", id);
		db.close();		
	}
	
	
	
	
	
	
	
	
	

}
