package com.sqliteavancado.persistencia;

import com.sqliteavancado.dominio.Pessoa;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MinhaHelper extends SQLiteOpenHelper {


	private static final String BANCO = "segundo.db";
	private static final int VERSAO = 1; 
	
	
	public MinhaHelper(Context context) {
		super(context, BANCO, null, VERSAO);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
	
		StringBuilder sql = new StringBuilder();
		sql.append("CREATE TABLE IF NOT EXISTS ");
		sql.append(Pessoa.PESSOA     + "( ");
		sql.append(Pessoa.ID         + " integer primary key autoincrement ");
		sql.append(Pessoa.NOME       + " text ");
		sql.append(Pessoa.SOBRENOME  + " text); ");
		
		db.execSQL(sql.toString());		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		//Em caso de atualização de versão escrever novo sql aqui.
	}

}
