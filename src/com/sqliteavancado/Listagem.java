package com.sqliteavancado;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.sqliteavancado.dominio.Pessoa;

public class Listagem extends ListActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ArrayList<Pessoa> pessoas = (ArrayList<Pessoa>) getIntent().getExtras()
				.getSerializable("pessoas");

	}
	
	
	
	public void altear(View v){
		
		Pessoa pessoa = new Pessoa();
		Intent intent = new Intent(this, TelaCadastro.class);
		intent.putExtra("pessoa", pessoa);
		
		startActivityForResult(intent, 0);
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
	}

}
